package controleur;

public class Avion {
	private int idAvion, nbPlaces;
	private String designation, constructeur;
	
	public Avion(int idAvion, int nbPlaces, String designation, String constructeur) {
		this.idAvion = idAvion;
		this.nbPlaces = nbPlaces;
		this.designation = designation;
		this.constructeur = constructeur;
	}
	
	public Avion(String designation, String constructeur, int nbPlaces) {
		this.idAvion = 0;
		this.nbPlaces = nbPlaces;
		this.designation = designation;
		this.constructeur = constructeur;
	}

	public int getIdAvion() {
		return idAvion;
	}

	public void setIdAvion(int idAvion) {
		this.idAvion = idAvion;
	}

	public int getNbPlaces() {
		return nbPlaces;
	}

	public void setNbPlaces(int nbPlaces) {
		this.nbPlaces = nbPlaces;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getConstructeur() {
		return constructeur;
	}

	public void setConstructeur(String constructeur) {
		this.constructeur = constructeur;
	}
}
