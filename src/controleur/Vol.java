package controleur;

public class Vol {
	private int idVol;
	private String designation, dateVol, heureVol;
	private int idPilote1, idPilote2, idAvion;
	
	
	public Vol(int idVol, String designation, String dateVol, String heureVol, int idPilote1, int idPilote2, int idAvion) {
		
		this.idVol = idVol;
		this.designation = designation;
		this.dateVol = dateVol;
		this.heureVol = heureVol;
		this.idPilote1 = idPilote1;
		this.idPilote2 = idPilote2;
		this.idAvion = idAvion;
	}
	
	public Vol(String designation, String dateVol, String heureVol, int idPilote1, int idPilote2, int idAvion) {

		this.idVol = 0;
		this.designation = designation;
		this.dateVol = dateVol;
		this.heureVol = heureVol;
		this.idPilote1 = idPilote1;
		this.idPilote2 = idPilote2;
		this.idAvion = idAvion;
	}

	public int getIdVol() {
		return idVol;
	}

	public void setIdVol(int idVol) {
		this.idVol = idVol;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getDateVol() {
		return dateVol;
	}

	public void setDateVol(String dateVol) {
		this.dateVol = dateVol;
	}

	public String getHeureVol() {
		return heureVol;
	}

	public void setHeureVol(String heureVol) {
		this.heureVol = heureVol;
	}

	public int getIdPilote1() {
		return idPilote1;
	}

	public void setIdPilote1(int idPilote1) {
		this.idPilote1 = idPilote1;
	}

	public int getIdPilote2() {
		return idPilote2;
	}

	public void setIdPilote2(int idPilote2) {
		this.idPilote2 = idPilote2;
	}

	public int getIdAvion() {
		return idAvion;
	}

	public void setIdAvion(int idAvion) {
		this.idAvion = idAvion;
	}
	
}
