package vue;

import java.awt.Color;

import javax.swing.JPanel;

//Panel de base, dont les autres panel hériteront
public abstract class PanelDeBase extends JPanel{
	
	public PanelDeBase(Color uneCouleur) {
		this.setBounds(40, 40, 800, 320);
		this.setBackground(uneCouleur);
		this.setLayout(null);
		this.setVisible(false);
	}
	
	
}
