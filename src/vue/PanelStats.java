package vue;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import modele.Modele;

public class PanelStats extends PanelDeBase{
	
	private JPanel panelCount = new JPanel();
	
	public PanelStats() {
		super(Color.cyan);
		
		//Construction du panel stats
		this.panelCount.setBounds(100, 100, 500, 200);
		this.panelCount.setLayout(new GridLayout(4,1));
		int nbPilotes = Modele.countPilote();
		int nbAvions = Modele.countAvion();
		int nbVols = Modele.countVol();
		int total = nbPilotes + nbAvions + nbVols;
		
		this.panelCount.add(new JLabel("Nombre de pilotes : " + nbPilotes));
		this.panelCount.add(new JLabel("Nombre d'avions : " + nbAvions));
		this.panelCount.add(new JLabel("Nombre de vols : " + nbVols));
		this.panelCount.add(new JLabel("Nombre d'opérations totales : " + total));
		this.panelCount.setVisible(true);
		this.add(panelCount);
	}
	
	public void actualiser() {
		this.panelCount.removeAll();
		
		int nbPilotes = Modele.countPilote();
		int nbAvions = Modele.countAvion();
		int nbVols = Modele.countVol();
		int total = nbPilotes + nbAvions + nbVols;
		
		this.panelCount.add(new JLabel("Nombre de pilotes : " + nbPilotes));
		this.panelCount.add(new JLabel("Nombre d'avions : " + nbAvions));
		this.panelCount.add(new JLabel("Nombre de vols : " + nbVols));
		this.panelCount.add(new JLabel("Nombre d'opérations totales : " + total));
		this.panelCount.setVisible(true);
	}
	
}
