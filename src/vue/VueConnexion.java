package vue;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controleur.AirFrance;
import controleur.User;

import javax.swing.JPasswordField;

public class VueConnexion extends JFrame implements ActionListener, KeyListener {
	
	private JPanel unPanel = new JPanel();
	private JButton btAnnuler = new JButton("Annuler");
	private JButton btSeConnecter = new JButton("Se connecter");
	private JTextField txtEmail = new JTextField();
	private JPasswordField txtMdp= new JPasswordField();
	
	public VueConnexion() {
		this.setTitle("Connexion - AirFrance Management");
		this.setBounds(200, 100, 700, 300);
		this.getContentPane().setBackground(Color.cyan);
		this.setResizable(false);
		this.setLayout(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//Construire le panel :
		this.unPanel.setBounds(350, 50, 300, 180);
		this.unPanel.setLayout(new GridLayout(3,2));
		this.unPanel.setBackground(Color.cyan);
		this.unPanel.add(new JLabel("Email : "));
		this.unPanel.add(txtEmail);
		this.unPanel.add(new JLabel("MDP : "));
		this.unPanel.add(txtMdp);
		this.unPanel.add(this.btAnnuler);
		this.unPanel.add(this.btSeConnecter);
		
		//Ajout d'une image
		ImageIcon uneImage = new ImageIcon("src/images/logo.png");
		JLabel logo = new JLabel(uneImage);
		logo.setBounds(0, 0, 0, 0);
		this.add(logo);
		
		this.add(this.unPanel); //Ajout du panel dans la fen�tre
		
		//Rendre les boutons �coutable :
		this.btAnnuler.addActionListener(this);
		this.btSeConnecter.addActionListener(this);
		
		this.txtEmail.addKeyListener(this);
		this.txtMdp.addKeyListener(this);
		
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == this.btAnnuler) { //Si on annule le formulaire
			this.txtEmail.setText("");
			this.txtMdp.setText("");
		} else if(e.getSource() == this.btSeConnecter) { //Si on se connecte
			traitement();
		}
	}
	
	//Traitement lorsqu'on se connecte
	public void traitement() {
		String email = this.txtEmail.getText();
		String mdp = new String (this.txtMdp.getPassword());
		
		User unUser = AirFrance.selectWhereUser(email, mdp);
		if(unUser == null) {
			JOptionPane.showMessageDialog(this,  "Veuillez v�rifier vos identifiants");
			this.txtEmail.setText("");
			this.txtMdp.setText("");
		}else {
			JOptionPane.showMessageDialog(this,  "Bienvenue "
					+ unUser.getPrenom() + " " + unUser.getNom()
					+"\n\n Vous avez le r�le : " + unUser.getRole());
			//Lancez l'application VueGenerale
			AirFrance.instancierVueGenerale(unUser);
			//On rend invisible la VueConnexion
			AirFrance.rendreVisibleVueConnexion(false);
			this.txtEmail.setText("");
			this.txtMdp.setText("");
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_ENTER) { //Quand on appuis sur la touche "entrer"
			traitement();
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

}
