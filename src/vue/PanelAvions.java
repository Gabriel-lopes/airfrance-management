package vue;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import controleur.Avion;
import controleur.Tableau;
import modele.Modele;

public class PanelAvions extends PanelDeBase implements ActionListener{

	private JPanel panelForm = new JPanel();
	private JButton btAnnuler = new JButton("Annuler");
	private JButton btEnregistrer = new JButton("Enregistrer");
	private JTextField txtDesignation = new JTextField();
	private JTextField txtConstructeur = new JTextField();
	private JTextField txtNbPlaces = new JTextField();

	
	private JTable uneTable;
	private JScrollPane uneScroll;
	private Tableau unTableau;
	
	public PanelAvions() {
		super(Color.green);
		//Construction du panelForm
		this.panelForm.setBounds(10, 40, 250, 240);
		this.panelForm.setLayout(new GridLayout(5,2)); // 5 Lignes / 2 colones
		this.panelForm.add(new JLabel ("Designation de l'avion : "));
		this.panelForm.add(this.txtDesignation);
		this.panelForm.add(new JLabel ("Constructeur de l'avion : "));
		this.panelForm.add(this.txtConstructeur);
		this.panelForm.add(new JLabel ("Nombre de places de l'avion : "));
		this.panelForm.add(this.txtNbPlaces);
		this.panelForm.add(btAnnuler);
		this.panelForm.add(btEnregistrer);
		this.add(panelForm);
		
		this.panelForm.setVisible(true);
		
		//Construction de la JScroll lister les pilotes
		String entetes [] = {"idAvion", "designation", "constructeur", "nbPlaces"};
		this.unTableau = new Tableau(getAvions(), entetes);
		
		
		this.uneTable = new JTable(this.unTableau);
		this.uneScroll = new JScrollPane(this.uneTable);
		this.uneScroll.setBounds(290, 40, 500, 240);
		this.add(this.uneScroll);
		
		//Traitement de la suppression
		this.uneTable.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				int numLigne = uneTable.getSelectedRow();
				if(e.getClickCount() == 2) {
					int idAvion = Integer.parseInt(unTableau.getValueAt(numLigne, 0).toString());
					int retour = JOptionPane.showConfirmDialog(null, "Voulez-vous supprimer l'avion ?", "Suppression de l'Avion", JOptionPane.YES_NO_OPTION);
					if(retour == 0) {
						Modele.supprimerAvion(idAvion); //deleteAvion
						unTableau.supprimerLigne(numLigne);
					}
				} 
				else if (e.getClickCount() == 1) {
					txtDesignation.setText(unTableau.getValueAt(numLigne, 1).toString());
					txtConstructeur.setText(unTableau.getValueAt(numLigne, 2).toString());
					txtNbPlaces.setText(unTableau.getValueAt(numLigne, 3).toString());
					btEnregistrer.setText("Modifier"); //On transforme le bouton enregistrer en modifier
				}
			}
		});
		
		//Bouton cliquable
		this.btAnnuler.addActionListener(this);
		this.btEnregistrer.addActionListener(this);
	}
	
	public Object [][] getAvions() {
		//methode qui transforme l'ArrayList des pilotes en une matrice [][]
		ArrayList<Avion> lesAvions = Modele.selectAllAvions();
		//le tableau de tableaux contient les pilotes suivis de leurs informations
		//Les pilotes = lesPilotes (arraylist) //// 5 = nombre de champs d'un pilote en comptant son id
		Object matrice [][] = new Object[lesAvions.size()][4];
		int i = 0;
		for(Avion unAvion : lesAvions) {
			matrice[i][0] = unAvion.getIdAvion();
			matrice[i][1] = unAvion.getDesignation();
			matrice[i][2] = unAvion.getConstructeur();
			matrice[i][3] = unAvion.getNbPlaces();
			i++;
		}
		return matrice;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == this.btAnnuler) {
			this.viderChamps();
		}
		else if(e.getSource() == this.btEnregistrer && e.getActionCommand().equals("Enregistrer")) {
			this.traitement(1);
		}
		else if(e.getSource() == this.btEnregistrer && e.getActionCommand().equals("Modifier")) {
			this.traitement(2);
		}
	}
	
	//Fonction qui vide les champs du formulaire si "Annuler"
	public void viderChamps() {
		this.txtDesignation.setText("");
		this.txtConstructeur.setText("");
		this.txtNbPlaces.setText("");
	}
	
	//Fonction qui traite l'enregistement
	public void traitement(int choix) {
		String designation = this.txtDesignation.getText();
		String constructeur = this.txtConstructeur.getText();
		int nbPlaces = Integer.parseInt(this.txtNbPlaces.getText());
		
		if(choix == 1) { //Si on veut enregistrer (ins�rer)
			
			//Instancier la classe Pilotes
			Avion unAvion = new Avion(designation, constructeur, nbPlaces);
			
			//Insertion BDD
			Modele.insertAvion(unAvion);
			
			//Message de r�ussite
			JOptionPane.showMessageDialog(this, "Insertion r�ussie");
			
			unAvion = Modele.selectWhereAvion(designation, constructeur);
			
			//Actualisation du tableau
			Object ligne[] = {unAvion.getIdAvion(), unAvion.getDesignation(), unAvion.getConstructeur(), unAvion.getNbPlaces()};
			this.unTableau.ajouterLigne(ligne);
			
			//Puis on vide le champs
			this.viderChamps();
		} 
		else { //Si le choix est �gal � 2 (modifier)
			int numLigne = uneTable.getSelectedRow();
			int idAvion = Integer.parseInt(unTableau.getValueAt(numLigne, 0).toString());
			//Instancier la classe Pilote
			Avion unAvion = new Avion(idAvion, nbPlaces, designation, constructeur);
			//Mise � jour dans la BDD
			Modele.updateAvion(unAvion);
			//Mise � jour de l'affichage
			Object ligne[] = {unAvion.getIdAvion(), unAvion.getDesignation(), unAvion.getConstructeur(), unAvion.getNbPlaces()};
			unTableau.modifierLigne(numLigne, ligne);
			JOptionPane.showMessageDialog(this, "Modification r�usssie");
		}
		this.btEnregistrer.setText("Enregistrer");
		//Vider le formulaire
		this.viderChamps();
	}

}
