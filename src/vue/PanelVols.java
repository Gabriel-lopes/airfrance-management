package vue;

import java.awt.Color;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controleur.Avion;
import controleur.Pilote;
import controleur.Vol;
import modele.Modele;

public class PanelVols extends PanelDeBase implements ActionListener
{
    private JPanel panelForm = new JPanel(); 
    private JButton btAnnuler = new JButton("Annuler"); 
    private JButton btEnregistrer = new JButton("Enregistrer");
    private JButton btRechercher = new JButton("Rechercher");
    private JTextField txtDesignation = new JTextField(); 
    private JTextField txtDateVol = new JTextField(); 
    private JTextField txtHeureVol = new JTextField(); 
    private JComboBox<String> txtidPilote1 = new JComboBox<String>();
    private JComboBox<String> txtidPilote2 = new JComboBox<String>();
    private JComboBox<String> txtidAvion = new JComboBox<String>();
    
    
    public PanelVols()
    {
        super (Color.yellow);  
        //Construction du panel Form
        this.panelForm.setBounds(10, 40, 250, 240);
        this.panelForm.setLayout(new GridLayout(7, 2));
        this.panelForm.add(new JLabel("Designation : ")); 
        this.panelForm.add(this.txtDesignation); 
        this.panelForm.add(new JLabel("Date du Vol : ")); 
        this.panelForm.add(this.txtDateVol);
        this.panelForm.add(new JLabel("Heure du Vol : ")); 
        this.panelForm.add(this.txtHeureVol);
        this.panelForm.add(new JLabel("Pilote 1: "));
        this.panelForm.add(this.txtidPilote1);
        this.panelForm.add(new JLabel("Pilote 2: "));
        this.panelForm.add(this.txtidPilote2);
        this.panelForm.add(new JLabel("Avion: "));
        this.panelForm.add(this.txtidAvion);
        this.panelForm.add(this.btAnnuler); 
        this.panelForm.add(this.btEnregistrer);
        this.panelForm.setVisible(true);
        this.add(this.panelForm);
        
        this.remplirCombo();
        //On rend les bouttons ecoutable
        this.btAnnuler.addActionListener(this);
        this.btEnregistrer.addActionListener(this);
        
        
    }
    
    //Remplir les Combo
    public void remplirCombo() {
            ArrayList<Pilote> lesPilotes = Modele.selectAllPilotes("");
            for(Pilote unPilote : lesPilotes)
            {
                this.txtidPilote1.addItem(unPilote.getIdPilote()+"-"+unPilote.getNom());
                this.txtidPilote2.addItem(unPilote.getIdPilote()+"-"+unPilote.getNom());
            }
            
            ArrayList<Avion> lesAvions = Modele.selectAllAvions();
            for(Avion unAvion : lesAvions)
            {
                this.txtidAvion.addItem(unAvion.getIdAvion()+"-"+unAvion.getDesignation());
            }

    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.btAnnuler)
        {
            viderChamps();
        }
        else if (e.getSource() == this.btEnregistrer)
        {
            traitement();
        }
        
    }
    
     public void viderChamps() {
            this.txtDesignation.setText("");
            this.txtDateVol.setText("");
            this.txtHeureVol.setText("");
        }
     
     public void traitement() {
         String Designation = this.txtDesignation.getText();
         String DateVol = this.txtDateVol.getText();
         String HeureVol = this.txtHeureVol.getText();
         //On recupere les ID Pilotes et Avions
         String chaine = this.txtidPilote1.getSelectedItem().toString();
         String tab[] = chaine.split("-");
         int idPilote1 = Integer.parseInt(tab[0]);
         
         chaine = this.txtidPilote2.getSelectedItem().toString();
         tab = chaine.split("-");
         int idPilote2 = Integer.parseInt(tab[0]);
         
         chaine = this.txtidAvion.getSelectedItem().toString();
         tab = chaine.split("-");
         int idAvion = Integer.parseInt(tab[0]);
         
         //Instantacion du Vol
         Vol unVol =  new Vol(Designation, DateVol, HeureVol, idPilote1, idPilote2, idAvion);
         Modele.insertVol(unVol);
         
         JOptionPane.showMessageDialog(this, "Insertion R�ussi");
     }

}