package vue;

import java.awt.Color;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import controleur.Pilote;
import controleur.Tableau;
import controleur.TableauBord;
import modele.Modele;

public class PanelBord extends PanelDeBase {
	
	private JTable uneTable;
	private Tableau unTableau;
	private JScrollPane uneScroll;
	
	public PanelBord() {
		super(Color.cyan);
		
		//Construction du panel tableau de bord
		String entetes [] = {"Nom", "Pr�nom", "Avion", "Vol", "Date"};
		this.unTableau = new Tableau(this.getTableauBord(""), entetes);
		this.uneTable = new JTable(this.unTableau);
		this.uneScroll = new JScrollPane(this.uneTable);
		this.uneScroll.setBounds(40, 20, 500, 350);
		this.add(this.uneScroll);
	}
	
	public Object [][] getTableauBord(String mot) {
		//methode qui transforme l'ArrayList des pilotes en une matrice [][]
		ArrayList<TableauBord> lesTableauxBords = Modele.selectAllTableauxBords(mot);
		//le tableau de tableaux contient les pilotes suivis de leurs informations
		//Les pilotes = lesPilotes (arraylist) //// 5 = nombre de champs d'un pilote en comptant son id
		Object matrice [][] = new Object[lesTableauxBords.size()][5];
		int i = 0;
		for(TableauBord unTab : lesTableauxBords) {
			matrice[i][0] = unTab.getNom();
			matrice[i][1] = unTab.getPrenom();
			matrice[i][2] = unTab.getAvion();
			matrice[i][3] = unTab.getVol();
			matrice[i][4] = unTab.getDate();
			i++;
		}
		return matrice;
	}

}
