package vue;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import controleur.AirFrance;
import controleur.User;

public class VueGenerale extends JFrame implements ActionListener{
	
	private JPanel panelMenu = new JPanel();
	private JButton btProfil = new JButton("Profil");
	private JButton btPilotes = new JButton("Pilotes");
	private JButton btAvions = new JButton("Avions");
	private JButton btVols = new JButton("Vols");
	private JButton btStats = new JButton("Statistiques");
	private JButton btBoard = new JButton("T-Board");
	private JButton btDeconnexion = new JButton("Deconnexion");
	
	private JPanel panelProfil = new JPanel();
	private PanelPilotes unPanelPilote = new PanelPilotes();
	private PanelAvions unPanelAvion = new PanelAvions();
	private PanelVols unPanelVol = new PanelVols();
	private PanelStats unPanelStats = new PanelStats();
	private PanelBord unPanelTableauBord = new PanelBord();
	
	public VueGenerale(User unUser) {
		this.setTitle("Administration des vols Air France");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(200, 100, 880, 400);
		this.setLayout(null);
		this.setResizable(false);
		this.getContentPane().setBackground(Color.cyan);
		
		//Construction du panel menu
		this.panelMenu.setLayout(new GridLayout(1,7));
		this.panelMenu.setBounds(20, 20, 780, 40);
		this.panelMenu.add(btProfil);
		this.panelMenu.add(btPilotes);
		this.panelMenu.add(btAvions);
		this.panelMenu.add(btVols);
		this.panelMenu.add(btStats);
		this.panelMenu.add(btBoard);
		this.panelMenu.add(btDeconnexion);
		this.panelMenu.setBackground(Color.cyan);
		this.add(this.panelMenu);
		
		//Construction du panel profil
		this.panelProfil.setBounds(100, 100, 500, 200);
		this.panelProfil.setLayout(new GridLayout(4,1));
		this.panelProfil.add(new JLabel("Nom user : " + unUser.getNom()));
		this.panelProfil.add(new JLabel("Pr�nom user : " + unUser.getPrenom()));
		this.panelProfil.add(new JLabel("Email user : " + unUser.getEmail()));
		this.panelProfil.add(new JLabel("R�le user : " + unUser.getRole()));
		this.panelProfil.setVisible(false);
		
		this.add(panelProfil);
		//Ajouts des panels dans la fen�tre
		this.add(this.unPanelPilote);
		this.add(this.unPanelAvion);
		this.add(this.unPanelVol);
		this.add(this.unPanelStats);
		this.add(this.unPanelTableauBord);
		
		//Rendre les bouton ecoutables
		this.btDeconnexion.addActionListener(this);
		this.btProfil.addActionListener(this);
		this.btPilotes.addActionListener(this);
		this.btAvions.addActionListener(this);
		this.btVols.addActionListener(this);
		this.btStats.addActionListener(this);
		this.btBoard.addActionListener(this);
		
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == btDeconnexion) {
			AirFrance.detruireVueGenerale();
			AirFrance.rendreVisibleVueConnexion(true);
		}
		else if(e.getSource() == btProfil) {
			this.panelProfil.setVisible(true);
			this.unPanelPilote.setVisible(false);
			this.unPanelAvion.setVisible(false);
			this.unPanelVol.setVisible(false);
			this.unPanelStats.setVisible(false);
			this.unPanelTableauBord.setVisible(false);
		}
		else if(e.getSource() == this.btPilotes) {
			this.unPanelPilote.setVisible(true);
			this.unPanelAvion.setVisible(false);
			this.unPanelVol.setVisible(false);
			this.panelProfil.setVisible(false);
			this.unPanelStats.setVisible(false);
			this.unPanelTableauBord.setVisible(false);
		}
		else if(e.getSource() == this.btAvions) {
			this.unPanelPilote.setVisible(false);
			this.unPanelAvion.setVisible(true);
			this.unPanelVol.setVisible(false);
			this.panelProfil.setVisible(false);
			this.unPanelStats.setVisible(false);
			this.unPanelTableauBord.setVisible(false);
		}
		else if(e.getSource() == this.btVols) {
			this.unPanelPilote.setVisible(false);
			this.unPanelAvion.setVisible(false);
			this.unPanelVol.setVisible(true);
			this.unPanelVol.remplirCombo();
			this.panelProfil.setVisible(false);
			this.unPanelStats.setVisible(false);
			this.unPanelTableauBord.setVisible(false);
		}
		else if(e.getSource() == this.btStats) {
			this.unPanelPilote.setVisible(false);
			this.unPanelAvion.setVisible(false);
			this.unPanelVol.setVisible(false);
			this.panelProfil.setVisible(false);
			this.unPanelStats.setVisible(true);
			
			this.unPanelStats.actualiser();
			this.unPanelStats.setVisible(true);
			this.unPanelTableauBord.setVisible(false);
		}
		else if(e.getSource() == this.btBoard) {
			this.unPanelPilote.setVisible(false);
			this.unPanelAvion.setVisible(false);
			this.unPanelVol.setVisible(false);
			this.panelProfil.setVisible(false);
			this.unPanelStats.setVisible(false);
			this.unPanelTableauBord.setVisible(true);
		}
	}

}
