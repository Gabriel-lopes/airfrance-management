package vue;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import controleur.Pilote;
import controleur.Tableau;
import modele.Modele;

public class PanelPilotes extends PanelDeBase implements ActionListener{
	
	private JPanel panelForm = new JPanel();
	private JButton btAnnuler = new JButton("Annuler");
	private JButton btEnregistrer = new JButton("Enregistrer");
	private JTextField txtNom = new JTextField();
	private JTextField txtPrenom = new JTextField();
	private JTextField txtEmail = new JTextField();
	private JTextField txtBip = new JTextField();

	
	private JTable uneTable;
	private JScrollPane uneScroll;
	private Tableau unTableau;
	
	
	private JTextField txtMot = new JTextField();
	private JButton btRechercher = new JButton("Rechercher");
	private JPanel panelRechercher = new JPanel();
	
	public PanelPilotes() {
		super(Color.gray); //Appel du paneldebase avec la couleur gris

		//Construction du panelForm
		this.panelForm.setBounds(10, 40, 250, 240);
		this.panelForm.setLayout(new GridLayout(5,2)); // 5 Lignes / 2 colones
		this.panelForm.add(new JLabel ("Nom du Pilote : "));
		this.panelForm.add(this.txtNom);
		this.panelForm.add(new JLabel ("Pr�nom du Pilote : "));
		this.panelForm.add(this.txtPrenom);
		this.panelForm.add(new JLabel ("Email du Pilote : "));
		this.panelForm.add(this.txtEmail);
		this.panelForm.add(new JLabel ("Bip du Pilote : "));
		this.panelForm.add(this.txtBip);
		this.panelForm.add(btAnnuler);
		this.panelForm.add(btEnregistrer);
		this.add(panelForm);
		
		//Construction du panel rechercher
		this.panelRechercher.setBounds(290, 40, 400, 30);
		this.panelRechercher.setLayout(new GridLayout(1,3)); // 3 Lignes / 1 colone
		this.panelRechercher.add(new JLabel ("Filtrer les Pilotes par : "));
		this.panelRechercher.add(this.txtMot);
		this.panelRechercher.add(this.btRechercher);
		this.add(this.panelRechercher);
		
		this.panelForm.setVisible(true);
		
		//Construction de la JScroll lister les pilotes
		String entetes [] = {"idPilote", "nom", "prenom", "email", "bip"};
		this.unTableau = new Tableau(getPilotes(""), entetes);
		
		
		this.uneTable = new JTable(this.unTableau);
		this.uneScroll = new JScrollPane(this.uneTable);
		this.uneScroll.setBounds(290, 100, 400, 180);
		this.add(this.uneScroll);
		
		//Traitement de la suppression
		this.uneTable.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				int numLigne = uneTable.getSelectedRow();
				if(e.getClickCount() == 2) {
					int idPilote = Integer.parseInt(unTableau.getValueAt(numLigne, 0).toString());
					int retour = JOptionPane.showConfirmDialog(null, "Voulez-vous supprimer le pilote ?", "Suppression de Pilote", JOptionPane.YES_NO_OPTION);
					if(retour == 0) {
						Modele.supprimerPilote(idPilote); //deletePilote
						unTableau.supprimerLigne(numLigne);
					}
				} 
				else if (e.getClickCount() == 1) {
					txtNom.setText(unTableau.getValueAt(numLigne, 1).toString());
					txtPrenom.setText(unTableau.getValueAt(numLigne, 2).toString());
					txtEmail.setText(unTableau.getValueAt(numLigne, 3).toString());
					txtBip.setText(unTableau.getValueAt(numLigne, 4).toString());
					btEnregistrer.setText("Modifier"); //On transforme le bouton enregistrer en modifier
				}
			}
		});
		
		//Bouton cliquable
		this.btAnnuler.addActionListener(this);
		this.btEnregistrer.addActionListener(this);
		this.btRechercher.addActionListener(this);
	}
	
	public Object [][] getPilotes(String mot) {
		//methode qui transforme l'ArrayList des pilotes en une matrice [][]
		ArrayList<Pilote> lesPilotes = Modele.selectAllPilotes(mot);
		//le tableau de tableaux contient les pilotes suivis de leurs informations
		//Les pilotes = lesPilotes (arraylist) //// 5 = nombre de champs d'un pilote en comptant son id
		Object matrice [][] = new Object[lesPilotes.size()][5];
		int i = 0;
		for(Pilote unPilote : lesPilotes) {
			matrice[i][0] = unPilote.getIdPilote();
			matrice[i][1] = unPilote.getNom();
			matrice[i][2] = unPilote.getPrenom();
			matrice[i][3] = unPilote.getEmail();
			matrice[i][4] = unPilote.getBip();
			i++;
		}
		return matrice;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == this.btAnnuler) {
			this.viderChamps();
		}
		else if(e.getSource() == this.btEnregistrer && e.getActionCommand().equals("Enregistrer")) {
			this.traitement(1);
		}
		else if(e.getSource() == this.btEnregistrer && e.getActionCommand().equals("Modifier")) {
			this.traitement(2);
		}
		else if(e.getSource() == this.btRechercher) {
			String mot = this.txtMot.getText();
			this.unTableau.setDonnees(this.getPilotes(mot));
		}
	}
	
	//Fonction qui vide les champs du formulaire si "Annuler"
	public void viderChamps() {
		this.txtNom.setText("");
		this.txtPrenom.setText("");
		this.txtEmail.setText("");
		this.txtBip.setText("");
	}
	
	//Fonction qui traite l'enregistement
	public void traitement(int choix) {
		String nom = this.txtNom.getText();
		String prenom = this.txtPrenom.getText();
		String email = this.txtEmail.getText();
		String bip = this.txtBip.getText();
		
		if(choix == 1) { //Si on veut enregistrer (ins�rer)
			
			//Instancier la classe Pilotes
			Pilote unPilote = new Pilote(nom, prenom, email, bip);
			
			//Insertion BDD
			Modele.insertPilote(unPilote);
			
			//Message de r�ussite
			JOptionPane.showMessageDialog(this, "Insertion r�ussie");
			
			unPilote = Modele.selectWherePilote(email, bip);
			
			//Actualisation du tableau
			Object ligne[] = {unPilote.getIdPilote(), unPilote.getNom(), unPilote.getPrenom(), unPilote.getEmail(), unPilote.getBip()};
			this.unTableau.ajouterLigne(ligne);
			
			//Puis on vide le champs
			this.viderChamps();
		} 
		else { //Si le choix est �gal � 2 (modifier)
			int numLigne = uneTable.getSelectedRow();
			int idPilote = Integer.parseInt(unTableau.getValueAt(numLigne, 0).toString());
			//Instancier la classe Pilote
			Pilote unPilote = new Pilote(idPilote, nom, prenom, email, bip);
			//Mise � jour dans la BDD
			Modele.updatePilote(unPilote);
			//Mise � jour de l'affichage
			Object ligne[] = {unPilote.getIdPilote(), unPilote.getNom(), unPilote.getPrenom(), unPilote.getEmail(), unPilote.getBip()};
			unTableau.modifierLigne(numLigne, ligne);
			JOptionPane.showMessageDialog(this, "Modification r�usssie");
		}
		this.btEnregistrer.setText("Enregistrer");
		//Vider le formulaire
		this.viderChamps();
	}

}
