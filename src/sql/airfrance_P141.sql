drop database if exists airfrance_p141;
create database airfrance_p141;
use airfrance_p141;

create table pilote (
	idPilote int(3) not null auto_increment,
    nom varchar(100) not null,
    prenom varchar(100) not null,
    email varchar(100) not null,
    bip varchar(100) not null,
    primary key(idPilote)
);

create table avion (
	idAvion int(3) not null auto_increment,
	designation varchar(100) not null,
    constructeur varchar(100) not null,
    nbPlaces int(5) not null,
    primary key(idAvion)
);

create table vol (
	idVol int(3) not null auto_increment,
	designation varchar(100) not null,
    dateVol date,
    heureVol time,
    idPilote1 int(3) not null,
    idPilote2 int(3) not null,
    idAvion int(3) not null,
    primary key(idVol),
    foreign key (idPilote1) references pilote(idPilote),
    foreign key (idPilote2) references pilote(idPilote),
    foreign key (idAvion) references avion(idAvion)
);

create table user (
    iduser int(3) not null auto_increment,
    nom varchar(50),
    prenom varchar(50),
    email varchar(100),
    mdp varchar(250),
    role enum("admin", "user"),
    primary key (iduser)
    );

insert into user values (null, "Lopes","Gabriel", "g@gmail.com", "123", "admin");
insert into user values (null, "Lopes","Fred", "f@gmail.com", "123", "user");

#View : pilote (nom, prenom), avion (designation), vol (designation, date)
CREATE VIEW tb AS (SELECT p.nom, p.prenom, a.designation as avion, v.designation as vol, v.datevol as date 
FROM pilote p, avion a, vol v
WHERE a.idavion = v.idavion
AND p.idpilote = v.idpilote1
);


    
    