package modele;

import java.sql.Statement;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;

import controleur.Avion;
import controleur.Pilote;
import controleur.TableauBord;
import controleur.User;
import controleur.Vol;

public class Modele {
	private static Bdd uneBdd = new Bdd("localhost", "airfrance_p141", "root", "");
	
	/************************** Gestion User ******************************/
	public static User selectWhereUser(String email, String mdp) {
		String requete = "Select * from user where email ='" + email + "' and mdp ='" + mdp + "';";
		User unUser = null;
		try {
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement(); //curseur d'execusion 
			//fetchAll en php : extraction des donn�es
			ResultSet unResultat = unStat.executeQuery(requete);
			//S'il y a un r�sultat :
			if(unResultat.next()) {
				unUser = new User (
					unResultat.getInt("iduser"),
					unResultat.getString("nom"),
					unResultat.getString("prenom"),
					unResultat.getString("email"),
					unResultat.getString("mdp"),
					unResultat.getString("role")
				);
			}
			unStat.close();
			uneBdd.seDeConnecter();
		}
		catch(SQLException exp) {
			System.out.println("Erreur d'execution de la requete : " + requete);
		}
		return unUser;
	}
	
	
	/************************** Gestion Pilote ******************************/
	//Ins�rer un pilote
	public static void insertPilote(Pilote unPilote)
	{
		String requete = "insert into pilote values(null, '" + unPilote.getNom()
		+ "','" + unPilote.getPrenom() + "','" + unPilote.getEmail() + "','" + unPilote.getBip() +"'); ";	
		try
		{
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement(); //curseur d'execusion 
			unStat.execute(requete); // execute comme php
			unStat.close();
			uneBdd.seDeConnecter();
		}
		catch(SQLException exp)
		{
			System.out.println("Erreur d'execution de la requete : " + requete);
		}	
	}
	
	//Afficher les pilotes
	public static ArrayList<Pilote> selectAllPilotes(String mot) {
		String requete = "";
		if(mot == "") {
			requete = "select * from Pilote; ";
		} else {
			requete = "select * from pilote where nom like '%"+ mot + "%' or prenom like '" + mot + "%'";
		}
		ArrayList<Pilote> lesPilotes = new ArrayList<Pilote>();
		try {
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement(); //curseur d'execusion 
			//fetchAll en php : extraction des donn�es
			ResultSet desResultats = unStat.executeQuery(requete);
			//tant qu'il y a des r�sultats :
			while(desResultats.next()) {
				Pilote unPilote = new Pilote (
					desResultats.getInt("idpilote"),
					desResultats.getString("nom"),
					desResultats.getString("prenom"),
					desResultats.getString("email"),
					desResultats.getString("bip")
				);
				//Ajouter l'instance pilote dans l'ArrayList
				lesPilotes.add(unPilote);
			}
			unStat.close();
		}
		catch(SQLException exp) {
			System.out.println("Erreur d'execution de la requete : " + requete);
		}
		return lesPilotes;
	}
	
	public static Pilote selectWherePilote(int idpilote) {
		String requete = "select * from Pilote where idpilote = " + idpilote;
		Pilote unPilote = null;
		try {
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement(); //curseur d'execusion 
			//fetchAll en php : extraction des donn�es
			ResultSet unResultat = unStat.executeQuery(requete);
			//S'il y a un r�sultat :
			if(unResultat.next()) {
				unPilote = new Pilote (
					unResultat.getInt("idpilote"),
					unResultat.getString("nom"),
					unResultat.getString("prenom"),
					unResultat.getString("email"),
					unResultat.getString("bip")
				);
			}
			unStat.close();
			uneBdd.seDeConnecter();
		}
		catch(SQLException exp) {
			System.out.println("Erreur d'execution de la requete : " + requete);
		}
		return unPilote;
	}
	
	public static Pilote selectWherePilote(String email, String bip) {
		String requete = "select * from Pilote where email = '" + email + "' and bip = '" + bip + "';";
		Pilote unPilote = null;
		try {
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement(); //curseur d'execusion 
			//fetchAll en php : extraction des donn�es
			ResultSet unResultat = unStat.executeQuery(requete);
			//S'il y a un r�sultat :
			if(unResultat.next()) {
				unPilote = new Pilote (
					unResultat.getInt("idpilote"),
					unResultat.getString("nom"),
					unResultat.getString("prenom"),
					unResultat.getString("email"),
					unResultat.getString("bip")
				);
			}
			unStat.close();
			uneBdd.seDeConnecter();
		}
		catch(SQLException exp) {
			System.out.println("Erreur d'execution de la requete : " + requete);
		}
		return unPilote;
	}
	
	public static void supprimerPilote(int idpilote) {
		String requete = "delete from pilote where idpilote = " + idpilote;
		try {
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement();
			unStat.execute(requete);
			unStat.close();
			uneBdd.seDeConnecter();
		}
		catch(SQLException exp) {
			System.out.println("Erreur d'execution de la requete " + requete);
		}
	}
	
	public static int countPilote() {
		int nb = 0;
		String requete = "select count(*) as nb from pilote";
		try {
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement();
			ResultSet unResultat = unStat.executeQuery(requete);
			if(unResultat.next()) {
				nb = unResultat.getInt("nb");
			}
			unStat.close();
			uneBdd.seDeConnecter();
		}
		catch(SQLException exp) {
			System.out.println("Erreur d'execution de la requete " + requete);
		}
		return nb;
	}
	
	public static void updatePilote(Pilote unPilote)
	{
		String requete = "update pilote set nom = '" + unPilote.getNom()
		+ "', prenom = '" + unPilote.getPrenom() + "', email = '" + unPilote.getEmail() + "', bip = '" + unPilote.getBip() 
		+"' where idpilote = " + unPilote.getIdPilote() + " ; ";	
		try
		{
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement(); //curseur d'execusion 
			unStat.execute(requete); // execute comme php
			unStat.close();
			uneBdd.seDeConnecter();
		}
		catch(SQLException exp)
		{
			System.out.println("Erreur d'execution de la requete : " + requete);
		}	
	}
	
	
	
	/************************** Gestion Avion ******************************/
	//Ins�rer un Avion
	public static void insertAvion(Avion unAvion)
	{
		String requete = "insert into avion values(null, '" + unAvion.getDesignation()
		+ "','" + unAvion.getConstructeur() + "','" + unAvion.getNbPlaces() +"'); ";	
		try
		{
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement(); //curseur d'execusion 
			unStat.execute(requete); // execute comme php
			unStat.close();
			uneBdd.seDeConnecter();
		}
		catch(SQLException exp)
		{
			System.out.println("Erreur d'execution de la requete : " + requete);
		}
	}
	
	//Afficher les avions
		
		public static ArrayList<Avion> selectAllAvions() {
			String requete = "select * from Avion; ";
			ArrayList<Avion> lesAvions = new ArrayList<Avion>();
			try {
				uneBdd.seConnecter();
				Statement unStat = uneBdd.getMaConnexion().createStatement(); //curseur d'execusion 
				//fetchAll en php : extraction des donn�es
				ResultSet desResultats = unStat.executeQuery(requete);
				//tant qu'il y a des r�sultats :
				while(desResultats.next()) {
					Avion unAvion = new Avion (
						desResultats.getInt("idavion"),
						desResultats.getInt("nbplaces"),
						desResultats.getString("designation"),
						desResultats.getString("constructeur")
					);
					//Ajouter l'instance pilote dans l'ArrayList
					lesAvions.add(unAvion);
				}
				unStat.close();
			}
			catch(SQLException exp) {
				System.out.println("Erreur d'execution de la requete : " + requete);
			}
			return lesAvions;
		}
		
		public static Avion selectWhereAvion(int idavion) {
			String requete = "select * from Avion where idavion = " + idavion;
			Avion unAvion = null;
			try {
				uneBdd.seConnecter();
				Statement unStat = uneBdd.getMaConnexion().createStatement(); //curseur d'execusion 
				//fetchAll en php : extraction des donn�es
				ResultSet unResultat = unStat.executeQuery(requete);
				//S'il y a un r�sultat :
				if(unResultat.next()) {
					unAvion = new Avion (
						unResultat.getInt("idavion"),
						unResultat.getInt("nbplaces"),
						unResultat.getString("designation"),
						unResultat.getString("constructeur")
					);
				}
				unStat.close();
				uneBdd.seDeConnecter();
			}
			catch(SQLException exp) {
				System.out.println("Erreur d'execution de la requete : " + requete);
			}
			return unAvion;
		}
		
		public static Avion selectWhereAvion(String designation, String constructeur) {
			String requete = "select * from Avion where designation = '" + designation + "' and constructeur = '" + constructeur + "';";
			Avion unAvion = null;
			try {
				uneBdd.seConnecter();
				Statement unStat = uneBdd.getMaConnexion().createStatement(); //curseur d'execusion 
				//fetchAll en php : extraction des donn�es
				ResultSet unResultat = unStat.executeQuery(requete);
				//S'il y a un r�sultat :
				if(unResultat.next()) {
					unAvion = new Avion (
						unResultat.getInt("idavion"),
						unResultat.getInt("nbplaces"),
						unResultat.getString("designation"),
						unResultat.getString("constructeur")
					);
				}
				unStat.close();
				uneBdd.seDeConnecter();
			}
			catch(SQLException exp) {
				System.out.println("Erreur d'execution de la requete : " + requete);
			}
			return unAvion;
		}
		
		public static void supprimerAvion(int idavion) {
			String requete = "delete from avion where idavion = " + idavion;
			try {
				uneBdd.seConnecter();
				Statement unStat = uneBdd.getMaConnexion().createStatement();
				unStat.execute(requete);
				unStat.close();
				uneBdd.seDeConnecter();
			}
			catch(SQLException exp) {
				System.out.println("Erreur d'execution de la requete " + requete);
			}
		}
		
		public static int countAvion() {
			int nb = 0;
			String requete = "select count(*) as nb from avion";
			try {
				uneBdd.seConnecter();
				Statement unStat = uneBdd.getMaConnexion().createStatement();
				ResultSet unResultat = unStat.executeQuery(requete);
				if(unResultat.next()) {
					nb = unResultat.getInt("nb");
				}
				unStat.close();
				uneBdd.seDeConnecter();
			}
			catch(SQLException exp) {
				System.out.println("Erreur d'execution de la requete " + requete);
			}
			return nb;
		}
		
		public static void updateAvion(Avion unAvion)
		{
			String requete = "update avion set designation = '" + unAvion.getDesignation()
			+ "', constructeur = '" + unAvion.getConstructeur() + "', nbplaces = '" + unAvion.getNbPlaces() 
			+"' where idavion = " + unAvion.getIdAvion() + " ; ";	
			try
			{
				uneBdd.seConnecter();
				Statement unStat = uneBdd.getMaConnexion().createStatement(); //curseur d'execusion 
				unStat.execute(requete); // execute comme php
				unStat.close();
				uneBdd.seDeConnecter();
			}
			catch(SQLException exp)
			{
				System.out.println("Erreur d'execution de la requete : " + requete);
			}
		}
		
		
		
		/************************** Gestion Vol ******************************/
	//Ins�rer un vol
	public static void insertVol(Vol unVol)
	{
		String requete = "insert into vol values(null, '" + unVol.getDesignation()
		+ "','" + unVol.getDateVol() + "','" + unVol.getHeureVol() + "','" + unVol.getIdPilote1() + "','" + unVol.getIdPilote2() + "','" + unVol.getIdAvion() +"'); ";	
		
		try
		{
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement(); //curseur d'execusion 
			unStat.execute(requete); // execute comme php
			unStat.close();
			uneBdd.seDeConnecter();
		}
		catch(SQLException exp)
		{
			System.out.println("Erreur d'execution de la requete : " + requete);
		}
	}
	
	public static ArrayList<Vol> selectAllVols() {
		String requete = "select * from Vol; ";
		ArrayList<Vol> lesVols = new ArrayList<Vol>();
		try {
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement(); //curseur d'execusion 
			//fetchAll en php : extraction des donn�es
			ResultSet desResultats = unStat.executeQuery(requete);
			//tant qu'il y a des r�sultats :
			while(desResultats.next()) {
				Vol unVol = new Vol (
					desResultats.getInt("idvol"),
					desResultats.getString("designation"),
					desResultats.getString("dateVol"),
					desResultats.getString("heureVol"),
					desResultats.getInt("idpilote1"),
					desResultats.getInt("idpilote2"),
					desResultats.getInt("idavion")
				);
				//Ajouter l'instance pilote dans l'ArrayList
				lesVols.add(unVol);
			}
			unStat.close();
		}
		catch(SQLException exp) {
			System.out.println("Erreur d'execution de la requete : " + requete);
		}
		return lesVols;
	}
	
	public static Vol selectWhereVol(int idvol) {
		String requete = "select * from Vol where idvol = " + idvol;
		Vol unVol = null;
		try {
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement(); //curseur d'execusion 
			//fetchAll en php : extraction des donn�es
			ResultSet unResultat = unStat.executeQuery(requete);
			//S'il y a un r�sultat :
			if(unResultat.next()) {
				unVol = new Vol (
					unResultat.getInt("idvol"),
					unResultat.getString("designation"),
					unResultat.getString("dateVol"),
					unResultat.getString("heureVol"),
					unResultat.getInt("idpilote1"),
					unResultat.getInt("idpilote2"),
					unResultat.getInt("idavion")
				);
			}
			unStat.close();
			uneBdd.seDeConnecter();
		}
		catch(SQLException exp) {
			System.out.println("Erreur d'execution de la requete : " + requete);
		}
		return unVol;
	}
	
	public static void supprimerVol(int idvol) {
		String requete = "delete from vol where idvol = " + idvol;
		try {
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement();
			unStat.execute(requete);
			unStat.close();
			uneBdd.seDeConnecter();;
		}
		catch(SQLException exp) {
			System.out.println("Erreur d'execution de la requete " + requete);
		}
	}
	
	public static int countVol() {
		int nb = 0;
		String requete = "select count(*) as nb from vol";
		try {
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement();
			ResultSet unResultat = unStat.executeQuery(requete);
			if(unResultat.next()) {
				nb = unResultat.getInt("nb");
			}
			unStat.close();
			uneBdd.seDeConnecter();
		}
		catch(SQLException exp) {
			System.out.println("Erreur d'execution de la requete " + requete);
		}
		return nb;
	}
	
	public static void updateVol(Vol unVol)
	{
		String requete = "update vol set designation = '" + unVol.getDesignation()
		+ "', datevol = '" + unVol.getDateVol() + "', heurevol = '" + unVol.getHeureVol() 
		+ "', idpilote1 = " + unVol.getIdPilote1() + ", idpilote2 = " + unVol.getIdPilote2() 
		+ ", idavion = " + unVol.getIdAvion() +" where idvol = " + unVol.getIdVol() + " ; ";	
		
		try
		{
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement(); //curseur d'execusion 
			unStat.execute(requete); // execute comme php
			unStat.close();
			uneBdd.seDeConnecter();
		}
		catch(SQLException exp)
		{
			System.out.println("Erreur d'execution de la requete : " + requete);
		}
	}
	
	/**************** Tableau de Bord ************************/
	
	public static ArrayList<TableauBord> selectAllTableauxBords(String mot) {
		String requete = "";
		if(mot == "") {
			requete = "select * from Pilote; ";
		} else {
			requete = "select * from pilote where nom like '%"+ mot + "%' or prenom like '" + mot + "%'";
		}
		ArrayList<TableauBord> lesTableauBords = new ArrayList<TableauBord>();
		try {
			uneBdd.seConnecter();
			Statement unStat = uneBdd.getMaConnexion().createStatement(); //curseur d'execusion 
			//fetchAll en php : extraction des donn�es
			ResultSet desResultats = unStat.executeQuery(requete);
			//tant qu'il y a des r�sultats :
			while(desResultats.next()) {
				TableauBord unTableauBord = new TableauBord (
					desResultats.getString("nom"),
					desResultats.getString("prenom"),
					desResultats.getString("avion"),
					desResultats.getString("vol"),
					desResultats.getString("date")
				);
				//Ajouter l'instance pilote dans l'ArrayList
				lesTableauBords.add(unTableauBord);
			}
			unStat.close();
		}
		catch(SQLException exp) {
			System.out.println("Erreur d'execution de la requete : " + requete);
		}
		return lesTableauBords;
	}
	
}
